% Copyright 2007 by Till Tantau
%
% This file may be distributed and/or modified
%
% 1. under the LaTeX Project Public License and/or
% 2. under the GNU Public License.
%
% See the file doc/licenses/LICENSE for more details.

\documentclass[aspectratio=169]{beamer}



% Setup appearance:

\usetheme{Darmstadt}
\usefonttheme[onlylarge]{structurebold}
\setbeamerfont*{frametitle}{size=\normalsize,series=\bfseries}
\setbeamertemplate{navigation symbols}{}
\addtobeamertemplate{navigation symbols}{}{%
    \usebeamerfont{footline}%
    \usebeamercolor[fg]{footline}%
    \hspace{1em}%
    \insertframenumber/\inserttotalframenumber
}


% Standard packages

\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{siunitx}
\usepackage{mathrsfs}
\usepackage{bm}
\usepackage{booktabs}
\usepackage{outlines}
\usepackage{changepage}
\captionsetup[figure]{labelfont={bf},name={ },labelsep=period}
\captionsetup[sub]{font=scriptsize,name={ }}

% Setup TikZ

\usepackage{tikz,tikz-3dplot}  % add fancy drawing
\usetikzlibrary{arrows}
\tikzstyle{block}=[draw opacity=0.7,line width=1.4cm]
\usepackage{pgfplots} % add fancy plots
\usetikzlibrary{pgfplots.groupplots} % for group plot
\pgfplotsset{compat=1.15}
\usetikzlibrary{positioning, shapes.geometric} % for schema plots
\usetikzlibrary{math}
\usetikzlibrary{arrows.meta}% ������

% Author, Title, etc.

\title[Block Partitioning and Perfect Phylogenies] 
{%
A spherical \& heterogeneous radiative transfer code for Titan's near-infrared remote sensing: Estimate the radiance and its gradient simultaneously
 %
}

\author[Zili HE]
{
  Zili HE, Sandrine Vinatier, Meso-Star
}

\institute[IMT Mines-Albi]
{
LESIA, Observatoire de Paris, CNRS (UMR-8109)\\
}

\date[December 7th, 2022]

% The main document

\begin{document}

\begin{frame}
  \centering
  \includegraphics[width=5cm]{./figures/obspm_LESIA.png}
  \titlepage
  \today
\end{frame}

\section{Context}
\begin{frame}[t]{Why do we need radiative transfer codes?}

\begin{columns}[T] % align columns

  \begin{column}{.58\textwidth}
  \centering
  \includegraphics[width=5.5cm]{figure/titan0.png}

  {\small [Vinatier et al. 2020]}

  \end{column}

  \begin{column}{.48\textwidth}

  \begin{itemize}
    \item Observations from JWST/Cassini (black)
    \item Simulation from RT code (red)
    \item Inversion process
  \end{itemize}

  \vspace{1cm}

  Radiative transfer code is a fondamental tool to study planets' atmosphere and surface.

  \end{column}
\end{columns} % align columns

\end{frame}

\begin{frame}[t]{Classical RT codes}

\begin{columns}
  \begin{column}{.38\textwidth}
  \centering
  \includegraphics[width=5.5cm]{./figures/disort_model.png}
  \textit{DISORT Report v1.1}
  \end{column}

  \begin{column}{.58\textwidth}
  \centering
  \begin{itemize}
    \item Parallel plan 
    \item Homogeneous layers
    \item Multi-scattering solved by Discrete-Ordinate-Method
  \end{itemize}
  \end{column}
\end{columns} % align columns
  
\end{frame}


\begin{frame}[t]{Why do we need spherical radiative transfer code on Titan ?}

 \begin{columns}[T] % align columns

  \begin{column}{.58\textwidth}
  \centering
  \includegraphics[width=5.5cm]{figure/titan1.png}

  \end{column}

  \begin{column}{.48\textwidth}

  \begin{itemize}
    \item Thick atmosphere 
    \item Strong scattering 
  \end{itemize}

  \vspace{1cm}

  Parallel-plan approximation works on Earth but not on Titan.

  \end{column}
\end{columns} % align columns

\end{frame}

\begin{frame}[t]{Why do we need heterogeneous radiative transfer code on Titan ?}

\begin{columns}[T] % align columns

  \begin{column}{.45\textwidth}
  \centering
  \includegraphics[width=3cm]{figure/pixel_adjacent1.png}
  \includegraphics[width=3cm]{figure/pixel_adjacent2.png}

  \textit{\small{MacKenzie, TTT 2023}}
  \end{column}

  \begin{column}{.45\textwidth}
  \centering

  \includegraphics[width=5cm]{figure/polar_cloud.png}

  \end{column}
  \end{columns} % align columns

  \vspace{1cm}

  \begin{itemize}
    \item The surface is heterogeneous.
    \item The atmosphere is heterogeneous.
  \end{itemize}

\end{frame}



\begin{frame}[t]{htrdr-planeto: initialised by Rad-Net}

\begin{columns}
  \begin{column}{.45\textwidth}
  \centering
  \includegraphics[width=5.5cm]{figure/config_sensitivity.png}
  \textit{DISORT Report v1.1}
  \end{column}

  \begin{column}{.45\textwidth}

  \begin{equation*}
    I_{\lambda} \equiv I_{\lambda} (\rho_1, \rho_2, \ldots, \kappa_{a_1}, \kappa_{s_1}, \kappa_{a_2}, \kappa_{s_2}, \ldots)
  \end{equation*}

  \begin{itemize}
    \item Spherical mesh 
    \item Multi-scattering solved by Monte-Carlo 
    \item Heterogeneous atmosphere and surface
  \end{itemize}

  \end{column}
\end{columns} % align columns
  
\end{frame}

\section{Result: Estimate the Radiance}

\begin{frame}[t]{Titan's images generated from GCM data}

  \vspace{0.5cm}

  \centering
  \includegraphics[width=6.5cm]{figure/titan2.png}
  \includegraphics[width=6.5cm]{figure/titan3.png}

  \vspace{0.5cm}

  [Meso-Star]
\end{frame}

\begin{frame}[t]{Titan's spectra at near-infrared region}

  \vspace{1cm}

  \begin{columns}
  \begin{column}{.38\textwidth}
  \centering
  \includegraphics[width=6cm]{figure/geom.png}
  \end{column}

  \begin{column}{.58\textwidth}
  \centering
  \begin{itemize}
    \item Mixed gas (Red)
      \begin{itemize}
        \item Pressure, Temperature 
        \item Absorption, k distribution: CH4, CO 
        \item Rayleigh scattering: CH4, N2
      \end{itemize}
    \item Haze (Blue)
      \begin{itemize}
        \item Haze [Doose et al. 2016]
      \end{itemize}
    \item Surface (Black)
      \begin{itemize}
        \item Lambertian surface [Doose et al. 2016]
      \end{itemize}

    \item overlap the gas mesh and haze mesh
  \end{itemize}
  \end{column}
\end{columns} % align columns
 
\end{frame}

\begin{frame}[t]{Titan's spectra at near-infrared region}
  \includegraphics[width=15cm]{figure/vims.png}
\end{frame}

\section{Result: Estimate the gradient}

\begin{frame}[t]{Why do we need the sensitivities (the gradient)?}
\begin{columns}[T] % align columns

  \begin{column}{.58\textwidth}
  \centering
  \includegraphics[width=5.5cm]{figure/config_hete_surface.jpeg}

  \end{column}

  \begin{column}{.48\textwidth}

  \begin{itemize}
    \item Sensitivity Analysis
    \item Inversion process needs the gradient
  \end{itemize}

  \end{column}
\end{columns} % align columns

\end{frame}

\begin{frame}[t]{Estimate $\vec{\nabla} I$}

\begin{columns}
  \begin{column}{.45\textwidth}
  \centering
  \includegraphics[width=5.5cm]{figure/config_sensitivity.png}
  \end{column}

  \begin{column}{.45\textwidth}

  \begin{equation*}
    I_{\lambda} \equiv I_{\lambda} (\rho_1, \rho_2, \ldots, \kappa_{a_1}, \kappa_{s_1}, \kappa_{a_2}, \kappa_{s_2}, \ldots)
  \end{equation*}

  Gradient of haze/surface properties:

  \begin{equation*}
    \vec{\nabla} I_{\lambda} \equiv [\partial_{\rho_1} I_{\lambda}, \partial_{\rho_2} I_{\lambda}, \ldots,\partial_{\kappa_{a1}} I,\partial_{\kappa_{s1}}, \ldots ] 
  \end{equation*}

  \end{column}
\end{columns} % align columns
\end{frame}

\begin{frame}[t]{$\partial_{\rho} I$ of each triangle of the surface}
\begin{figure}[ht!]
 \begin{columns}
  \begin{column}{.48\textwidth}
        \includegraphics[width=6cm]{figure/rho_3_933.08.png}
        \caption{$\lambda = 933.08 \; nm$ }
  \end{column}
  \begin{column}{.48\textwidth}
        \includegraphics[width=6cm]{figure/rho_249_5022.4.png}
        \caption{$\lambda = 5022.4 \; nm$ }
  \end{column}
\end{columns} % align columns
\end{figure}
\end{frame}

\begin{frame}[t]{Visualize the optical paths}
\begin{figure}[ht!]
 \begin{columns}
  \begin{column}{.48\textwidth}
        \includegraphics[width=7cm]{figure/path_3.png}
        \caption{$\lambda = 933.08 \; nm$}
  \end{column}
  \begin{column}{.48\textwidth}
        \includegraphics[width=7cm]{figure/path_249.png}
        \caption{$\lambda = 5022.4 \; nm$ }
  \end{column}
\end{columns} % align columns
\end{figure}
\end{frame}

\begin{frame}[t]{$\partial_{\rho} I$ of each triangle of the surface}
  \centering
        \includegraphics[width=6cm]{figure/sens_rho_plot.pdf}

        The sensitive zone is extended to hundreds of kilometers!
\end{frame}

\begin{frame}[t]{$\partial_{\kappa_a} I$ and $\partial_{\kappa_s} I$ of each cell}
\begin{figure}[ht!]
 \begin{columns}
  \begin{column}{.48\textwidth}
        \includegraphics[width=8cm]{figure/kaks_3_933.078.pdf}
        \caption{$\lambda = 933.08 \; nm$}
  \end{column}
  \begin{column}{.48\textwidth}
        \includegraphics[width=8cm]{figure/kaks_249_5022.4.pdf}
        \caption{$\lambda = 5022.4 \; nm$ }
  \end{column}
\end{columns} % align columns
\end{figure}
\end{frame}

\begin{frame}[t]{$\partial_{\kappa_a} I$, $\partial_{\kappa_s} I$ and $\partial_{\kappa_e} I$ of each layer}
  \centering
  \includegraphics[width=8cm]{figure/vertical_map.pdf}
  
\end{frame}

\section{Method}

\begin{frame}[t]{Content}
 \begin{itemize}
  \item Modeling a radiative system: Radiative Transfer Equation + Boundary Conditions
  \item Monte Carlo method
  \item The Monte Carlo estimator of the radiance (the solution)
  \item The Monte Carlo estimator of the gradient: Differentiate the solution
 \end{itemize} 
\end{frame}

\begin{frame}[t]{Radiative Transfer Equation (RTE)}
  \textbf{Radiative transfer equation} (RTE) describes the transport for intensity $I\;[\SI{}{\watt \per \metre^2 \cdot \steradian}]$.

  \begin{block}{RTE in the atmosphere under the LTE assumption$^*$}
   \begin{equation*}
    {\small
    \begin{array}{lllr}
      \vec{\omega} \cdot \vec{\nabla} I(\vec{x},\vec{\omega}) &=& -k_{a} I(\vec{x},\vec{\omega})& absorption(-) \\
                                                                  & & - k_{d} I(\vec{x},\vec{\omega}) & scattering(-)\\
                                                                  & &  + k_{a}I^{eq}\left(T(\vec{x})\right) & emission(+)\\
                                                                  & &+ k_{d} \int_{\Omega^{\prime}}\mathcal{P}(-\vec{\omega}^{\prime}|-\vec{\omega})d\vec{\omega}^{\prime} I(\vec{x},\vec{\omega}^{\prime})& scattering(+)
    \end{array}
  }
  \end{equation*}
  \end{block}
  {\small $^*$ local thermodynamic equilibrium}

\begin{figure}
\centering
  \pause \begin{tikzpicture}[scale = 0.5]
    \draw[->,black] (-1,0)--(5,0);
    \draw[->,orange,ultra thick] (0,0)--(1,0); %-> L^+
    \filldraw[gray] (3,0) circle (3pt) node[anchor=north,yshift=20,color=gray]{\small absorption}; % node x = \ddot{\pi}
    \draw[->,gray,ultra thick] (3,0)--(4,0); %-> L^+
  \end{tikzpicture} 
  \pause \begin{tikzpicture}[scale = 0.5]
    \draw[->,black] (-1,0)--(5,0);
    \draw[->,orange,ultra thick] (0,0)--(1,0); %-> L^+
    \filldraw[red] (3,0) circle (3pt);
    \filldraw[red] (3,0) circle (3pt) node[anchor=north,yshift=30,xshift=-10,color=gray]{scattering(-)}; % node x = \ddot{\pi}
    \draw[->,gray,ultra thick] (3,0)--(4,0); %-> L^+
    \draw[->,orange,ultra thick] (3,0)--(3.75,0.75); %-> L^+
  \end{tikzpicture} 
  \pause \begin{tikzpicture}[scale = 0.5]
    \draw[->,black] (-1,0)--(5,0);
    \draw[->,orange,ultra thick] (0,0)--(1,0); %-> L^+
    \filldraw[red] (3,0) circle (3pt);
    \filldraw[red] (3,0) circle (3pt) node[anchor=north,yshift=20,color=orange]{\small emission}; % node x = \ddot{\pi}
    \draw[->,orange,ultra thick] (3,0)--(4,0); %-> L^+
  \end{tikzpicture} 
  \pause \begin{tikzpicture}[scale = 0.5]
    \draw[->,black] (-1,0)--(5,0);
    \draw[->,orange,ultra thick] (0,0)--(1,0); %-> L^+
    \filldraw[red] (3,0) circle (3pt);
    \filldraw[red] (3,0) circle (3pt) node[anchor=north,yshift=20,color=orange]{\small scattering(+)}; % node x = \ddot{\pi}
    \draw[->,orange,ultra thick] (3,0)--(4,0); %-> L^+
    \draw[->,orange,ultra thick] (2.25,0.25)--(3,0); %-> L^+
  \end{tikzpicture} 

\end{figure}

\end{frame}

\begin{frame}[t]{Boundary conditions: Top of Atmosphere \& Surface}


 \begin{columns}

  \begin{column}{.38\textwidth}
  \centering
  \includegraphics[width=4cm]{figure/config_sensitivity.png}
  \end{column}

  \begin{column}{.58\textwidth}
    {
    \small
  \begin{equation*}
    I(\vec{x},\vec{u}) = \int_{2\pi} I(\vec{x},\vec{u}^{\prime}) f_b(\vec{x},\vec{u}^{\prime}| \vec{u})d \vec{u}^{\prime}, \;\;\; \vec{x} \in \mathbb{G}, \; \vec{u} \in \mathbb{H}^2
    \label{eq:re}
  \end{equation*}

  \begin{equation*}
    I(\vec{x},\vec{u}) = I_{planck} \mathcal{H}(\vec{u} \in \Omega_{s})  , \;\;\; \vec{x} \in \mathrm{TOA}, \; \vec{u} \in \mathbb{H}^2 
    \label{eq:bcI2}
  \end{equation*}
  
}
  \end{column}
  \end{columns}
  
\end{frame}

\begin{frame}[t]{Monte Carlo method}

  Estimate an \textbf{expected value} following the \textit{law of large numbers}:
  \begin{equation*}
   \int_{\mathscr{D}} f(x) dx = \mathbb{E}(A) = \int_{\mathscr{D}} \underbrace{p(x)}_{pdf} \underbrace{\frac{f(x)}{p(x)}}_{weight} dx \approx \frac{1}{n_{mc}} \sum_{i=1}^{n_{mc}} \frac{f(x_i)}{p(x_i)}
  \end{equation*}
  \begin{itemize}
    \item[$\cdot$]
      $pdf$: probability density function
    \item[$\cdot$]
      $n_{mc}$: Number of realizations
  \end{itemize}

  Following the \textit{Central limit theorem}, an associated standard deviation is provided. 

\end{frame}

\begin{frame}[t]{The Monte Carlo estimator of the radiance}

 \begin{columns}

  \begin{column}{.48\textwidth}
 \begin{equation*}
  I(\vec{x},\vec{u}) = \int_{\mathscr{D}_{\Gamma}} p_{\Gamma}(\gamma)w_{I}(\gamma) d\gamma = \mathbb{E}[w_{I}(\Gamma)] 
  \label{eq:IE}
\end{equation*}

\begin{equation*}
  \mathbb{E}[w_{I}(\Gamma)] \approx \frac{1}{N_{mc}} \sum_{i = 1}^{N_{mc}} w_I(\gamma_i)
  \label{eq:IE2}
\end{equation*}
  \end{column}

  \begin{column}{.48\textwidth}
  \centering
  \includegraphics[width=6cm]{figure/config_I_algo.png}
  \end{column}

\end{columns} % align columns

See more details in paper being prepared [Z.He et al. 2024].
 
\end{frame}

\begin{frame}[t]{How to estimate the gradient? Finite difference is forbidden ! [Gobet,2016]}

 \begin{columns}

  \begin{column}{.48\textwidth}
  \centering

  \begin{equation*}
    \frac{\partial I}{\partial \rho} \approx \frac{I(\rho+\delta \rho)-I(\rho)}{\delta \rho}
  \end{equation*}

  \end{column}

  \begin{column}{.48\textwidth}
  \centering
  \includegraphics[width=5cm]{figure/nodf.png}
  \end{column}

\end{columns} % align columns
  
\end{frame}
\begin{frame}[t]{How to estimate the gradient? Differentiate the integral formulation!}
 \begin{columns}
  \begin{column}{.48\textwidth}
\begin{equation*}
  I(\rho) = \int_{\mathscr{D}_{\Gamma}} p_{\Gamma}(\gamma, \rho)w_{I}(\gamma,\rho) d\gamma = \mathbb{E}[w_{I}(\Gamma(\rho),\rho)]
\end{equation*}

\begin{equation*}
  s(\rho) \equiv \frac{\partial I}{\partial \rho}
\end{equation*}

\begin{equation*}
  s(\rho) = \int_{\mathscr{D}_{\Gamma}} p_{\Gamma}(\gamma, \rho)w_{s}(\gamma,\rho) d\gamma = \mathbb{E}[w_{s}(\Gamma(\rho),\rho)]
\end{equation*}

\begin{equation*}
w_s(\gamma,\rho) = \frac{\partial_{\rho} p_{\Gamma}(\gamma,\rho) w_I(\gamma,\rho)}{p_{\Gamma}(\gamma,\rho)} + \partial_{\rho} w_I(\gamma,\rho)
\end{equation*}

  \end{column}

  \begin{column}{.35\textwidth}
    \centering
    \includegraphics[width=3cm]{figure/yessens.png}
  \end{column}
 \end{columns} % align columns
\end{frame}

\begin{frame}[t]{Vectorized Monte Carlo}
  \begin{equation*}
  \begin{bmatrix}
    I(\vec{x},\vec{\omega},\rho) \\
    s_{\rho_1}(\vec{x},\vec{\omega},\rho)\\
    s_{\rho_2}(\vec{x},\vec{\omega},\rho)\\
    \cdots\\
    s_{\kappa_{a1}}(\vec{x},\vec{\omega},\rho)\\
    s_{\kappa_{s1}}(\vec{x},\vec{\omega},\rho)\\
    \cdots\\
  \end{bmatrix}
  = \mathbb{E} 
  \begin{bmatrix}
  w_I(\Gamma,\vec{x},\vec{\omega},\rho)\\
  w_{s\rho1}(\Gamma,\vec{x},\vec{\omega},\rho)\\
  \cdots\\
  \end{bmatrix}
  \approx \frac{1}{N} \sum_{i=1}^{N} 
  \begin{bmatrix}
  w_I(\gamma_{i},\vec{x},\vec{\omega},\rho)\\
  w_{s\rho1}(\gamma_{i},\vec{x},\vec{\omega},\rho)\\
  \cdots\\
  \end{bmatrix}
  \end{equation*}

  However, the geometric parameters are more complicated (phd thesis [He. 2023], [Lapeyre,2021]).

\end{frame}

\section{Conclusion \& Prospectives}
\begin{frame}[t]{Conclusion }
  \begin{itemize}
    \item We developed a spherical, heterogeneous RT code.
    \item The radiance and its gradient are estimated simultaneously using the same sampled optical paths and the number of sensitivities is not limited.
    \item We performed a sensitivity analysis about Titan's atmosphere:
      \begin{itemize}
        \item The sensitive zone of surface albedo to the radiance can be extended to hundreds of kilometers due to scattering effect. 
        \item The sensitive zone of $\kappa_a$ and  $\kappa_s$ is around the line of sight. 
        \item The sensitivity of $\kappa_e$ can be negative or positive for different wavelengths and altitude of the atmosphere. 
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[t]{Prospectives}
  \begin{itemize}
    \item Integrate the gradient estimation into the inversion process.
    \item Apply this method to other planets: Mars, Venus, Jupiter, etc.
    \item Two papers are being prepared: 
      \begin{itemize}
        \item Radiance estimation 
        \item Radiance and sensitivities estimation 
      \end{itemize}
  \end{itemize}
\end{frame}

\appendix
\section{Different betwen PP and Spherical code}

\begin{frame}[t]{Different of pp and sphere in multi-scattering}
  The difference of PP and sphere comes from the multi-scattering in two geometries.

  \centering
  \includegraphics[width=6cm]{figure/config_pp_sphere.png}

  $\tau = \tau^*$ but  $I \neq I^*$

\end{frame}

\begin{frame}[t]{Different of pp and sphere in multi-scattering}
  We seperate the intensity by the number of scattering: 

  \begin{equation*}
    I = \sum_{i=0}^{\infty} I_i
  \end{equation*}
  \begin{equation*}
    I^* = \sum_{i=0}^{\infty} I_i^*
  \end{equation*}

  $I_0 = I^*_0 = 0$ because the only source is the sun

  $I_1 = I^*_1$

  \centering
  \includegraphics[width=7cm]{figure/i1.png}
\end{frame}

\begin{frame}[t]{Different of pp and sphere in multi-scattering}
  $I_2 \neq I^*_2$ and $I_j \neq I^*_j, \forall i >= 2$

  \includegraphics[width=10cm]{figure/i2.png}
\end{frame}

\end{document}
