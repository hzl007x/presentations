% Copyright 2007 by Till Tantau
%
% This file may be distributed and/or modified
%
% 1. under the LaTeX Project Public License and/or
% 2. under the GNU Public License.
%
% See the file doc/licenses/LICENSE for more details.

\documentclass[aspectratio=169]{beamer}



% Setup appearance:

\usetheme{Darmstadt}
\usefonttheme[onlylarge]{structurebold}
\setbeamerfont*{frametitle}{size=\normalsize,series=\bfseries}
\setbeamertemplate{navigation symbols}{}
\addtobeamertemplate{navigation symbols}{}{%
    \usebeamerfont{footline}%
    \usebeamercolor[fg]{footline}%
    \hspace{1em}%
    \insertframenumber/\inserttotalframenumber
}


% Standard packages

\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{siunitx}
\usepackage{mathrsfs}
\usepackage{bm}
\usepackage{booktabs}
\usepackage{outlines}
\usepackage{changepage}
\captionsetup[figure]{labelfont={bf},name={ },labelsep=period}
\captionsetup[sub]{font=scriptsize,name={ }}

% Setup TikZ

\usepackage{tikz,tikz-3dplot}  % add fancy drawing
\usetikzlibrary{arrows}
\tikzstyle{block}=[draw opacity=0.7,line width=1.4cm]
\usepackage{pgfplots} % add fancy plots
\usetikzlibrary{pgfplots.groupplots} % for group plot
\pgfplotsset{compat=1.15}
\usetikzlibrary{positioning, shapes.geometric} % for schema plots
\usetikzlibrary{math}
\usetikzlibrary{arrows.meta}% ������

% Author, Title, etc.

\title[Block Partitioning and Perfect Phylogenies] 
{%
  Simulate near-infrared spectrum of the Huygens landing site on Titan: DISORT vs. htrdr 
 %
}

\author[Zili HE]
{
  Zili HE
}

\institute[IMT Mines-Albi]
{
  LESIA
}

\date[December 7th, 2022]



% The main document

\begin{document}

\begin{frame}
  \centering
  \includegraphics[width=5cm]{./figures/obspm_LESIA.png}
  \titlepage
\end{frame}

\end{document}


