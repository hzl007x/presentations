% Copyright 2007 by Till Tantau
%
% This file may be distributed and/or modified
%
% 1. under the LaTeX Project Public License and/or
% 2. under the GNU Public License.
%
% See the file doc/licenses/LICENSE for more details.

\documentclass[aspectratio=169]{beamer}

% Setup appearance:
\usetheme{Darmstadt}
\usefonttheme[onlylarge]{structurebold}
\setbeamerfont*{frametitle}{size=\normalsize,series=\bfseries}
\setbeamertemplate{navigation symbols}{}
\addtobeamertemplate{navigation symbols}{}{%
    \usebeamerfont{footline}%
    \usebeamercolor[fg]{footline}%
    \hspace{1em}%
    \insertframenumber/\inserttotalframenumber
}

% Standard packages

\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{siunitx}
\usepackage{mathrsfs}
\usepackage{bm}
\usepackage{booktabs}
\usepackage{outlines}
\usepackage{changepage}
\captionsetup[figure]{labelfont={bf},name={ },labelsep=period}
\captionsetup[sub]{font=scriptsize,name={ }}

% Setup TikZ

\usepackage{tikz,tikz-3dplot}  % add fancy drawing
\usetikzlibrary{arrows}
\tikzstyle{block}=[draw opacity=0.7,line width=1.4cm]
\usepackage{pgfplots} % add fancy plots
\usetikzlibrary{pgfplots.groupplots} % for group plot
\pgfplotsset{compat=1.15}
\usetikzlibrary{positioning, shapes.geometric} % for schema plots
\usetikzlibrary{math}
\usetikzlibrary{arrows.meta}% ������

% Author, Title, etc.

\title[Block Partitioning and Perfect Phylogenies] 
{%
  Sensitivity analysis of Titan's atmosphere
 %
}

\author[Zili HE]
{
  Zili HE, Sandrine Vinatier, Bruno Bezard
}

\institute[IMT Mines-Albi]
{
LESIA, Observatoire de Paris, CNRS (UMR-8109)\\
}

% The main document

\begin{document}

\begin{frame}
  \centering
  \includegraphics[width=5cm]{./figures/obspm_LESIA.png}
  \titlepage
\end{frame}

\section{Context}

\begin{frame}[t]{htrdr-planeto-sensitivity: Estimate $I$ and $\vec{\nabla} I$ simultaneously}

\begin{columns}
  \begin{column}{.45\textwidth}
  \centering
  \includegraphics[width=5.5cm]{./figures/config_sensitivity.png}
  \textit{DISORT Report v1.1}
  \end{column}

  \begin{column}{.45\textwidth}
  \begin{equation*}
    I_{\lambda} \equiv I_{\lambda} (\rho_1, \rho_2, \ldots, \kappa_{a_1}, \kappa_{s_1}, \kappa_{a_2}, \kappa_{s_2}, \ldots)
  \end{equation*}

  \begin{equation*}
    \vec{\nabla} I_{\lambda} \equiv [\partial_{\rho_1} I_{\lambda}, \partial_{\rho_2} I_{\lambda}, \ldots,\partial_{\kappa_{a1}} I,\partial_{\kappa_{s1}}, \ldots ] 
  \end{equation*}

  We choose to estimate $\partial_{\rho *} I$,  $\partial_{\kappa_{a}^*}$ and  $\partial_{\kappa_{s}^*}$.

  \begin{equation*}
  \partial_{\kappa_e^*} I = \partial_{\kappa_s^*} I \times \alpha  + \partial_{\kappa_a^*} I \times (1-\alpha)
  \end{equation*}
  \begin{equation*}
  \partial_{n^*} I = \partial_{\kappa_e^*} I \times \sigma_e
  \end{equation*}

  \hfill

  \end{column}
\end{columns} % align columns
  
\end{frame}

\section{Method}
\begin{frame}[t]{Finite difference is forbidden ! [Gobet,2016]}

 \begin{columns}

  \begin{column}{.48\textwidth}
  \centering

  \begin{equation*}
    \frac{\partial I}{\partial \rho} \approx \frac{I(\rho+\delta \rho)-I(\rho-\delta \rho)}{2 \delta \rho}
  \end{equation*}

  \end{column}

  \begin{column}{.48\textwidth}
  \centering
  \includegraphics[width=5cm]{./figures/nodf.png}
  \end{column}

\end{columns} % align columns
  
\end{frame}
\begin{frame}[t]{Differentiate the integral formulation}
 \begin{columns}
  \begin{column}{.48\textwidth}
\begin{equation*}
  I(\rho) = \int_{\mathscr{D}_{\Gamma}} p_{\Gamma}(\gamma, \rho)\hat{w}_{I}(\gamma,\rho) d\gamma = \mathbb{E}[\hat{w}_{I}(\Gamma(\rho),\rho)]
\end{equation*}

\begin{equation*}
  s(\rho) \equiv \frac{\partial I}{\partial \rho}
\end{equation*}

\begin{equation*}
  s(\rho) = \int_{\mathscr{D}_{\Gamma}} p_{\Gamma}(\gamma, \rho)\hat{w}_{s}(\gamma,\rho) d\gamma = \mathbb{E}[\hat{w}_{s}(\Gamma(\rho),\rho)]
\end{equation*}

\begin{equation*}
\hat{w}_s(\gamma,\rho) = \frac{\partial_{\rho} p_{\Gamma}(\gamma,\rho) \hat{w}_I(\gamma,\rho)}{p_{\Gamma}(\gamma,\rho)} + \partial_{\rho} \hat{w}_I(\gamma,\rho)
\end{equation*}

  \end{column}

  \begin{column}{.35\textwidth}
    \centering
    \includegraphics[width=3cm]{./figures/yessens.png}
  \end{column}
 \end{columns} % align columns
\end{frame}

\begin{frame}[t]{Vectorized Monte Carlo}
  \begin{equation*}
  \begin{bmatrix}
    I(\vec{x},\vec{\omega},\rho) \\
    s_{\rho_1}(\vec{x},\vec{\omega},\rho)\\
    s_{\rho_2}(\vec{x},\vec{\omega},\rho)\\
    \cdots\\
    s_{\kappa_{a1}}(\vec{x},\vec{\omega},\rho)\\
    s_{\kappa_{s1}}(\vec{x},\vec{\omega},\rho)\\
    \cdots\\
  \end{bmatrix}
  = \mathbb{E} 
  \begin{bmatrix}
  \hat{w}_I(\Gamma,\vec{x},\vec{\omega},\rho)\\
  \hat{w}_{s\rho1}(\Gamma,\vec{x},\vec{\omega},\rho)\\
  \cdots\\
  \end{bmatrix}
  \approx \frac{1}{N} \sum_{i=1}^{N} 
  \begin{bmatrix}
  \hat{w}_I(\gamma_{i},\vec{x},\vec{\omega},\rho)\\
  \hat{w}_{s\rho1}(\gamma_{i},\vec{x},\vec{\omega},\rho)\\
  \cdots\\
  \end{bmatrix}
  \end{equation*}

  However, the geometric parameters are more subtle (phd thesis [He. 2023], [Lapeyre,2021]).

  \hfill

  Perspective: inversion of Titan topographic map by VIMS, CIRS, etc.

\end{frame}

\begin{frame}[t]{Intensity path, sensitivity path}

  See paper (working on) 
\end{frame}

\section{Model}
\begin{frame}[t]{Model}

 \begin{columns}
  \begin{column}{.48\textwidth}
  \centering
  \includegraphics[width=7cm]{./figures/htrdr_sp_kagas.png}
  \end{column}

  \begin{column}{.48\textwidth}
  \centering
  \begin{itemize}
    \item spherical meshing 
    \item Multi-scattering solved by Monte-Carlo
    \item Gas 
      \begin{itemize}
        \item Pressure, Temperature 
        \item Absorption, k distribution: CH4, CO 
        \item Rayleigh scattering: CH4, N2
      \end{itemize}
    \item Haze [Doose et al. 2016]
    \item overlap the gas mesh and haze mesh
  \end{itemize}
  \end{column}
\end{columns} % align columns
  
\end{frame}

\begin{frame}[t]{Observation angle}
\begin{figure}[ht!]
        \includegraphics[width=8cm]{figures/observation_angle.png}
\end{figure}
\end{frame}

\section{Results}

\begin{frame}[t]{$\partial_{\rho} I$}
\begin{figure}[ht!]
        \includegraphics[width=6cm]{./figures/rho_249_5022.4.png}
        \caption{$\lambda = 5022.4 \; nm$ }
\end{figure}
\end{frame}

\begin{frame}[t]{$\partial_{\rho} I$}
\begin{figure}[ht!]
        \includegraphics[width=6cm]{./figures/rho_3_933.08.png}
        \caption{$\lambda = 933.08 \; nm$ }
\end{figure}
\end{frame}

\begin{frame}[t]{$\partial_{\rho} I$}
\begin{figure}[ht!]
        \includegraphics[width=7cm]{./figures/sens_rho_plot.pdf}
\end{figure}
\end{frame}

\begin{frame}[t]{$Optical path$}
\begin{figure}[ht!]
        \includegraphics[width=7cm]{./figures/path_249.png}
        \caption{$\lambda = 5022.4 \; nm$}
\end{figure}
\end{frame}

\begin{frame}[t]{Optical path}
\begin{figure}[ht!]
        \includegraphics[width=7cm]{figures/path_159.png}
        \caption{$\lambda = 3512.84 \; nm$}
\end{figure}
\end{frame}

\begin{frame}[t]{Optical path}
\begin{figure}[ht!]
        \includegraphics[width=7cm]{./figures/path_3.png}
        \caption{$\lambda = 933.08 \; nm$}
\end{figure}
\end{frame}

\begin{frame}[t]{$\partial_{\kappa_a} I$ and $\partial_{\kappa_s} I$ $\lambda = 5022.4 \; nm$ }
\begin{figure}[ht!]
        \includegraphics[width=9cm]{figures/kaks_249_5022.4.pdf}
        \caption{$\lambda = 5022.4 \; nm$}
\end{figure}
\end{frame}

\begin{frame}[t]{$\partial_{\kappa_a} I$ and $\partial_{\kappa_s} I$}
\begin{figure}[ht!]
        \includegraphics[width=6cm]{figures/path_ks_sens.png}
\end{figure}
\end{frame}

\begin{frame}[t]{$\partial_{\kappa_a} I$ and $\partial_{\kappa_s} I$ $\lambda = 3512.84 \; nm$}
\begin{figure}[ht!]
        \includegraphics[width=9cm]{figures/kaks_159_3512.84.pdf}
        \caption{$\lambda = 3512.84 \; nm$}
\end{figure}
\end{frame}

\begin{frame}[t]{$\partial_{\kappa_a} I$ and $\partial_{\kappa_s} I$ $\lambda = 933.08 \; nm$}
\begin{figure}[ht!]
        \includegraphics[width=9cm]{figures/kaks_3_933.078.pdf}
        \caption{$\lambda = 933.08 \; nm$}
\end{figure}
\end{frame}

\end{document}


