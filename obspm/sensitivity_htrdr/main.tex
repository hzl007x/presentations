% Copyright 2007 by Till Tantau
%
% This file may be distributed and/or modified
%
% 1. under the LaTeX Project Public License and/or
% 2. under the GNU Public License.
%
% See the file doc/licenses/LICENSE for more details.

\documentclass[aspectratio=169]{beamer}



% Setup appearance:
\usetheme{Darmstadt}
\usefonttheme[onlylarge]{structurebold}
\setbeamerfont*{frametitle}{size=\normalsize,series=\bfseries}
\setbeamertemplate{navigation symbols}{}
\addtobeamertemplate{navigation symbols}{}{%
    \usebeamerfont{footline}%
    \usebeamercolor[fg]{footline}%
    \hspace{1em}%
    \insertframenumber/\inserttotalframenumber
}

% Standard packages

\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{siunitx}
\usepackage{mathrsfs}
\usepackage{bm}
\usepackage{booktabs}
\usepackage{outlines}
\usepackage{changepage}
\captionsetup[figure]{labelfont={bf},name={ },labelsep=period}
\captionsetup[sub]{font=scriptsize,name={ }}

% Setup TikZ

\usepackage{tikz,tikz-3dplot}  % add fancy drawing
\usetikzlibrary{arrows}
\tikzstyle{block}=[draw opacity=0.7,line width=1.4cm]
\usepackage{pgfplots} % add fancy plots
\usetikzlibrary{pgfplots.groupplots} % for group plot
\pgfplotsset{compat=1.15}
\usetikzlibrary{positioning, shapes.geometric} % for schema plots
\usetikzlibrary{math}
\usetikzlibrary{arrows.meta}% ������

% Author, Title, etc.

\title[Block Partitioning and Perfect Phylogenies] 
{%
 htrdr-planeto toward inversion: sensitivities estimation 
 %
}

\author[Zili HE]
{
  Zili HE, Vincent Emeyt, Vincent Forest, Sandrine Vinatier
}

\institute[IMT Mines-Albi]
{
LESIA, Observatoire de Paris, CNRS (UMR-8109)\\
Meso-Star
}

% The main document

\begin{document}

\begin{frame}
  \centering
  \includegraphics[width=5cm]{./figures/obspm_LESIA.png}
  \titlepage
\end{frame}

\section{Context}
\begin{frame}[t]{Limitations of DISORT + INVERSION: pixel adjacency; polar cloud}

\begin{columns}[T] % align columns

  \begin{column}{.45\textwidth}
  \centering
  \includegraphics[width=3cm]{./figures/pixel_adjacent1.png}
  \includegraphics[width=3cm]{./figures/pixel_adjacent2.png}

  \textit{\small{MacKenzie, TTT 2023}}
  \end{column}

  \begin{column}{.45\textwidth}
  \centering

  \includegraphics[width=5cm]{./figures/polar_cloud.png}

  \end{column}
  \end{columns} % align columns

  \begin{itemize}
    \item htrdr-planeto:
    \begin{itemize}
    \item Heterogeneous surface and atmosphere 
    \item Spherical code + multi-scattering  
    \end{itemize}
  \item htrdr-planeto-sensitivity
    \begin{itemize}
    \item Surface and atmosphere properties gradient 
    \end{itemize}
  \end{itemize}

\end{frame}

\begin{frame}[t]{htrdr-planeto-sensitivity: Estimate $I$ and $\vec{\nabla} I$ simultaneously}

\begin{columns}
  \begin{column}{.45\textwidth}
  \centering
  \includegraphics[width=5.5cm]{./figures/config_sensitivity.png}
  \textit{DISORT Report v1.1}
  \end{column}

  \begin{column}{.45\textwidth}
  \begin{equation*}
    I_{\lambda} \equiv I_{\lambda} (\rho_1, \rho_2, \ldots, \kappa_{a_1}, \kappa_{s_1}, \kappa_{a_2}, \kappa_{s_2}, \ldots)
  \end{equation*}

  \begin{equation*}
    \vec{\nabla} I_{\lambda} \equiv [\partial_{\rho_1} I_{\lambda}, \partial_{\rho_2} I_{\lambda}, \ldots] 
  \end{equation*}

%  \begin{equation*}
%    \mathbf{J_{I}} = 
%    \begin{bmatrix}
%      \partial_{\rho_1} I_{\lambda_1}, \partial_{\rho_2} I_{\lambda_1}, \cdots \\
%      \partial_{\rho_1} I_{\lambda_2}, \partial_{\rho_2} I_{\lambda_2}, \cdots \\
%      \partial_{\rho_1} I_{\lambda_3}, \partial_{\rho_2} I_{\lambda_3}, \cdots \\
%      \cdots \\
%    \end{bmatrix}
%  \end{equation*}

  \hfill

  if $\kappa_{a_1} \equiv \kappa_{a_1}(c_{HCN},\delta_{HCN})$, $\frac{\partial I_{\lambda}}{\partial c_{HCN}} = \frac{\partial I_{\lambda}}{\partial \kappa_{a_1}} \frac{\partial \kappa_{a_1}}{\partial c_{HCN}}$

  \end{column}
\end{columns} % align columns
  
\end{frame}

\section{Method}
\begin{frame}[t]{Finite difference is forbidden ! [Gobet,2016]}

 \begin{columns}

  \begin{column}{.48\textwidth}
  \centering

  \begin{equation*}
    \frac{\partial I}{\partial \rho} \approx \frac{I(\rho+\delta \rho)-I(\rho-\delta \rho)}{2 \delta \rho}
  \end{equation*}

  \end{column}

  \begin{column}{.48\textwidth}
  \centering
  \includegraphics[width=5cm]{./figures/nodf.png}
  \end{column}

\end{columns} % align columns
  
\end{frame}
\begin{frame}[t]{Differentiate the integral formulation}
 \begin{columns}
  \begin{column}{.48\textwidth}
\begin{equation*}
  I(\rho) = \int_{\mathscr{D}_{\Gamma}} p_{\Gamma}(\gamma, \rho)\hat{w}_{I}(\gamma,\rho) d\gamma = \mathbb{E}[\hat{w}_{I}(\Gamma(\rho),\rho)]
\end{equation*}

\begin{equation*}
  s(\rho) \equiv \frac{\partial I}{\partial \rho}
\end{equation*}

\begin{equation*}
  s(\rho) = \int_{\mathscr{D}_{\Gamma}} p_{\Gamma}(\gamma, \rho)\hat{w}_{s}(\gamma,\rho) d\gamma = \mathbb{E}[\hat{w}_{s}(\Gamma(\rho),\rho)]
\end{equation*}

\begin{equation*}
\hat{w}_s(\gamma,\rho) = \frac{\partial_{\rho} p_{\Gamma}(\gamma,\rho) \hat{w}_I(\gamma,\rho)}{p_{\Gamma}(\gamma,\rho)} + \partial_{\rho} \hat{w}_I(\gamma,\rho)
\end{equation*}

  \end{column}

  \begin{column}{.35\textwidth}
    \centering
    \includegraphics[width=3cm]{./figures/yessens.png}
  \end{column}
 \end{columns} % align columns
\end{frame}

\begin{frame}[t]{Vectorized Monte Carlo}
  \begin{equation*}
  \begin{bmatrix}
    I(\vec{x},\vec{\omega},\rho) \\
    s_{\rho_1}(\vec{x},\vec{\omega},\rho)\\
    s_{\rho_2}(\vec{x},\vec{\omega},\rho)\\
    \cdots\\
    s_{\kappa_{a1}}(\vec{x},\vec{\omega},\rho)\\
    s_{\kappa_{s1}}(\vec{x},\vec{\omega},\rho)\\
    \cdots\\
  \end{bmatrix}
  = \mathbb{E} 
  \begin{bmatrix}
  \hat{w}_I(\Gamma,\vec{x},\vec{\omega},\rho)\\
  \hat{w}_{s\rho1}(\Gamma,\vec{x},\vec{\omega},\rho)\\
  \cdots\\
  \end{bmatrix}
  \approx \frac{1}{N} \sum_{i=1}^{N} 
  \begin{bmatrix}
  \hat{w}_I(\gamma_{i},\vec{x},\vec{\omega},\rho)\\
  \hat{w}_{s\rho1}(\gamma_{i},\vec{x},\vec{\omega},\rho)\\
  \cdots\\
  \end{bmatrix}
  \end{equation*}

  However, the geometric parameters are more subtle (phd thesis [He. 2023], [Lapeyre,2021]).

  \hfill

  Perspective: inversion of Titan topographic map by VIMS, CIRS, etc.

\end{frame}

\begin{frame}[t]{Intensity path, sensitivity path}

  See document.
\end{frame}

\section{Result}
\begin{frame}[t]{Homogeneous surface configuration + Huygens atmosphere, $\frac{\partial I}{\partial \rho}$}
  \centering
  \includegraphics[width=7cm]{./figures/config_homo_surface.jpeg}
\end{frame}

\begin{frame}[t]{Homogeneous surface, Huygens atmosphere, $I \equiv I (\lambda |\rho = 0.5)$, $\frac{\partial I}{\partial \rho} (\lambda)$}
  \centering
  \includegraphics[width=12cm]{./figures/res_homo_surface.pdf}
\end{frame}

\begin{frame}[t]{Heterogeneous surface configuration + Huygens atmosphere, $\frac{\partial I}{\partial \rho}$}
  \centering
  \includegraphics[width=7cm]{./figures/config_hete_surface.jpeg}
\end{frame}

\begin{frame}[t]{Heterogeneous surface configuration + Huygens atmosphere, $\frac{\partial I}{\partial \rho}$}
  \centering
  \includegraphics[width=10cm]{./figures/res_hete_surface.pdf}
\end{frame}

\section{Publication strategy}
\begin{frame}[t]{Paper 1:A heterogeneous spherical radiative transfer code: htrdr-planeto}

  Positioning: What can be simulated by htrdr-planeto but not by 1D codes.

  The main point: illustrate the 3D effects 

  ``3D effects'' includes: 
  \begin{itemize}
    \item curvature effect on multi-scattering: differences between htrdr-planeto and pseudo spherical codes.
    \item Heterogeneous atmosphere
    \item Heterogeneous surface
  \end{itemize}

  On Titan, the 3D effects are significant. Same cases on Venus and Mars ?

\end{frame}

\begin{frame}[t]{Paper 2: Simultaneous estimations of sensitivities}

  Short communication on JQRST

  What are presented above + one more point:

  \begin{itemize}
    \item Estimate the sensitivities of the parameters of cloud/haze optical models. E.g. Particle size, particle distribution.
  \end{itemize}

  Perspective and discussion:

  \begin{itemize}
    \item Why VIMS data is not used to inverse the topographic map? It is feasible for 1D codes? 
    \item Sensitivity on the cloud form. 
    \item Sensitivity on the surface topography. 
  \end{itemize}

\end{frame}

\begin{frame}[t]{Paper 3 \& 4: Coupling the sensitivities and inversion algorithms}

\begin{itemize}
  \item Surface albedos on Huygens landing site and Selk crater.
  \item Spectral analyse of Pole clouds using VIMS data.
\end{itemize}

\end{frame}





\section{Appendix: Validations}

\begin{frame}[t]{Homogeneous surface, 1 layer, optically thin}
  \centering
  \includegraphics[width=7cm]{./figures/config_homo_surface.jpeg}
\end{frame}

\begin{frame}[t]{Homogeneous surface, 1 layer, optically thin}
    \centering
    \includegraphics[width=12cm]{./figures/valid_homo_surface1.pdf}
\end{frame}

\begin{frame}[t]{Homogeneous surface, 1 layer, optically thick}
    \centering
    \includegraphics[width=12cm]{./figures/valid_homo_surface2.pdf}
\end{frame}

\begin{frame}[t]{Heterogeneous surface configuration + Huygens atmosphere, $\frac{\partial I}{\partial \rho}$}
  \centering
  \includegraphics[width=7cm]{./figures/config_hete_surface.jpeg}
\end{frame}

\begin{frame}[t]{Heterogeneous surface, 1 layer, $\tau = 1$, SENS4}
    \centering
    \includegraphics[width=12cm]{./figures/valid_hete_surface.pdf}
\end{frame}

\begin{frame}[t]{Sensitivity on $\kappa_{a1}$ and $\kappa_{s1}$}
    \centering
    \includegraphics[width=8cm]{./figures/config_sensitivity_ka.png}
    \includegraphics[width=8cm]{./figures/config_sensitivity_ks.png}
\end{frame}

\begin{frame}[t]{Sensitivity on $\kappa_{a1}$}
    \centering
    \includegraphics[width=12cm]{./figures/valid_ka.pdf}
\end{frame}

\begin{frame}[t]{Sensitivity on $\kappa_{s1}$}
    \centering
    \includegraphics[width=12cm]{./figures/valid_ks.pdf}
\end{frame}

\begin{frame}[t]{Sensitivity on cloud properties}
    \centering
    \includegraphics[width=8cm]{./figures/config_sensitivity_cloud.png}
\end{frame}

\begin{frame}[t]{Sensitivity on cloud properties}
    \centering
    \includegraphics[width=12cm]{./figures/valid_ka_cloud.pdf}
\end{frame}

\begin{frame}[t]{Sensitivity on cloud properties}
    \centering
    \includegraphics[width=12cm]{./figures/valid_ks_cloud.pdf}
\end{frame}






\end{document}


