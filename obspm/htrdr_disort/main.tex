% Copyright 2007 by Till Tantau
%
% This file may be distributed and/or modified
%
% 1. under the LaTeX Project Public License and/or
% 2. under the GNU Public License.
%
% See the file doc/licenses/LICENSE for more details.

\documentclass[aspectratio=169]{beamer}



% Setup appearance:

\usetheme{Darmstadt}
\usefonttheme[onlylarge]{structurebold}
\setbeamerfont*{frametitle}{size=\normalsize,series=\bfseries}
\setbeamertemplate{navigation symbols}{}
\addtobeamertemplate{navigation symbols}{}{%
    \usebeamerfont{footline}%
    \usebeamercolor[fg]{footline}%
    \hspace{1em}%
    \insertframenumber/\inserttotalframenumber
}


% Standard packages

\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{siunitx}
\usepackage{mathrsfs}
\usepackage{bm}
\usepackage{booktabs}
\usepackage{outlines}
\usepackage{changepage}
\captionsetup[figure]{labelfont={bf},name={ },labelsep=period}
\captionsetup[sub]{font=scriptsize,name={ }}

% Setup TikZ

\usepackage{tikz,tikz-3dplot}  % add fancy drawing
\usetikzlibrary{arrows}
\tikzstyle{block}=[draw opacity=0.7,line width=1.4cm]
\usepackage{pgfplots} % add fancy plots
\usetikzlibrary{pgfplots.groupplots} % for group plot
\pgfplotsset{compat=1.15}
\usetikzlibrary{positioning, shapes.geometric} % for schema plots
\usetikzlibrary{math}
\usetikzlibrary{arrows.meta}% ������

% Author, Title, etc.

\title[Block Partitioning and Perfect Phylogenies] 
{%
  Simulate near-infrared spectrum of the Huygens landing site on Titan: DISORT vs. htrdr 
 %
}

\author[Zili HE]
{
  Zili HE, Bruno Bezard, Vincent Emeyt, Vincent Forest, Sandrine Vinatier
}

\institute[IMT Mines-Albi]
{
LESIA, Observatoire de Paris, CNRS (UMR-8109)\\
Meso-Star
}

%\date[December 7th, 2022]

% The main document

\begin{document}

\begin{frame}
  \centering
  \includegraphics[width=5cm]{./figures/obspm_LESIA.png}
  \titlepage
  \today
\end{frame}

\section{Context}
\begin{frame}[t]{The observation}

\begin{columns}[T] % align columns

  \begin{column}{.38\textwidth}
  \centering
  \includegraphics[width=4cm]{./figures/landing_site.png}

  \textit{NASA}
  \end{column}

  \begin{column}{.58\textwidth}
  \centering
  \begin{itemize}
    \item CH4 transmittance windows at 930nm, 1070nm, 1299nm, 1570nm, 2043nm 
    \item CO transmittance windows at 5000nm
    \item Important haze scattering 
  \end{itemize}
  \end{column}
\end{columns} % align columns

  \centering
  \includegraphics[width=12cm]{./figures/vims.png}
 
\end{frame}

\section{DISORT vBruno}
\begin{frame}[t]{The model}

\begin{columns}
  \begin{column}{.38\textwidth}
  \centering
  \includegraphics[width=5.5cm]{./figures/disort_model.png}
  \textit{DISORT Report v1.1}
  \end{column}

  \begin{column}{.58\textwidth}
  \centering
  \begin{itemize}
    \item Parallel plan 
    \item Multi-scattering solved by Discrete-Ordinate-Method
    \item Data interpolated on layers (couches):
      \begin{itemize}
        \item Pressure, Temperature 
        \item Absorption, k distribution: CH4, CO 
        \item Rayleigh scattering: CH4, N2
        \item Haze [Doose et al. 2016]
      \end{itemize}
  \end{itemize}
  \end{column}
\end{columns} % align columns
  
\end{frame}

\begin{frame}[t]{Results}
  \includegraphics[width=18cm]{./figures/disort_huygens.png}

\begin{itemize}
  \item Calculation time: about 15 mins
  \item Biased after 2700nm
\end{itemize}
\end{frame}

\section{htrdr PP}
\begin{frame}[t]{Model}

 \begin{columns}
  \begin{column}{.48\textwidth}
  \centering
  \includegraphics[width=3cm]{./figures/htrdr_pp_kagas.png}
  \includegraphics[width=3cm]{./figures/htrdr_pp_kshaze.png}
  \end{column}

  \begin{column}{.48\textwidth}
  \centering
  \begin{itemize}
    \item Near - Parallel plan 
    \item Multi-scattering solved by Monte-Carlo (no discretesation)
    \item Gas data interpolated exactly the same as disort:
      \begin{itemize}
        \item Pressure, Temperature 
        \item Absorption, k distribution: CH4, CO 
        \item Rayleigh scattering: CH4, N2
      \end{itemize}
    \item Haze data interpolated almost the same as disort
      \begin{itemize}
        \item Haze [Doose et al. 2016]
      \end{itemize}
    \item overlap the gas mesh and haze mesh
  \end{itemize}
  \end{column}
\end{columns} % align columns
  
\end{frame}

\begin{frame}[t]{Results}
  \includegraphics[width=14cm]{./figures/htrdr_pp_res1.png}
  \includegraphics[width=14cm]{./figures/htrdr_pp_res2.png}
\end{frame}

\section{htrdr Sphere}
\begin{frame}[t]{Model}

 \begin{columns}
  \begin{column}{.48\textwidth}
  \centering
  \includegraphics[width=7cm]{./figures/htrdr_sp_kagas.png}
  \end{column}

  \begin{column}{.48\textwidth}
  \centering
  \begin{itemize}
    \item Spherical mesh 
    \item Multi-scattering solved by Monte-Carlo (no discretesation)
    \item Data interpolated exactly the same as htrdr pp for each column
  \end{itemize}
  \end{column}
\end{columns} % align columns
  
\end{frame}

\begin{frame}[t]{Results, discussion}
  \includegraphics[width=14cm]{./figures/htrdr_sp_res.png}

\begin{itemize}
  \item Almost the same DATA interpolation 
  \item Different solver for htrdr and disort 
  \item Different geometry for htrdr sphere and htrdr pp / disort 
  \item \textcolor{red} {The spectrum of htrdr sphere is a little lower ? }
    \begin{itemize}
      \item \textcolor{red} {Mesh resolution }
      \item \textcolor{red} {Hypothese pp vs. sphere }
    \end{itemize}
\end{itemize}

\end{frame}

\section{Mesh testing}

\begin{frame}[t]{Case1: transparent medium}
\begin{columns}[T] % align columns

  \begin{column}{.48\textwidth}
  \centering
    \includegraphics[width=5cm]{./figures/sphere_10.png}

    Nphi = Ntheta = 10
  \end{column}

  \begin{column}{.48\textwidth}
  \centering
  \includegraphics[width=5cm]{./figures/sphere_100.png}

    Nphi = Ntheta = 100
  \end{column}
\end{columns} % align columns
\end{frame}

\begin{frame}[t]{Case1: transparent medium}
  \includegraphics[width=14cm]{./figures/transparent.pdf}
\end{frame}

\begin{frame}[t]{Case2: absorption with analytical solution}
\begin{columns}[T] % align columns

  \begin{column}{.48\textwidth}
  \centering
    \includegraphics[width=5cm]{./figures/atm_10.png}

    Nphi = Ntheta = 10
  \end{column}

  \begin{column}{.48\textwidth}
  \centering
  \includegraphics[width=5cm]{./figures/atm_100.png}

    Nphi = Ntheta = 100
  \end{column}
\end{columns} % align columns
\end{frame}

\begin{frame}[t]{Case2: absorption with analytical solution}
  \includegraphics[width=14cm]{./figures/absorbant.pdf}
\end{frame}

\begin{frame}[t]{Case3: absorption + scattering with the result of DISORT}
  \includegraphics[width=14cm]{./figures/scattering.pdf}
\end{frame}

\section{Sphere -> PP}

\begin{frame}[t]{A big sphere -> PP}
  \includegraphics[width=14cm]{./figures/sphere_to_pp.pdf}
\end{frame}

\begin{frame}[t]{Different of pp and sphere in multi-scattering}
  The difference of PP and sphere comes from the multi-scattering in two geometries.

  \centering
  \includegraphics[width=6cm]{./figures/config_pp_sphere.png}

  $\tau = \tau^*$ but  $I \neq I^*$

\end{frame}

\begin{frame}[t]{Different of pp and sphere in multi-scattering}
  We seperate the intensity by the number of scattering: 

  \begin{equation*}
    I = \sum_{i=0}^{\infty} I_i
  \end{equation*}
  \begin{equation*}
    I^* = \sum_{i=0}^{\infty} I_i^*
  \end{equation*}

  $I_0 = I^*_0 = 0$ because the only source is the sun

  $I_1 = I^*_1$

  \centering
  \includegraphics[width=7cm]{./figures/i1.png}
\end{frame}

\begin{frame}[t]{Different of pp and sphere in multi-scattering}
  $I_2 \neq I^*_2$ and $I_j \neq I^*_j, \forall i >= 2$

  \includegraphics[width=10cm]{./figures/i2.png}
\end{frame}

\section{Conclusion}
\begin{frame}[t]{Conclusion}
  \begin{itemize}
    \item htrdr-sphere and htrdr-PP are validated with DISORT
    \item The different simulation results between spherical atmosphere and pp atmosphere is from the multi-scattering.
  \end{itemize}
\end{frame}

\begin{frame}[t]{Prospectives}
  \begin{itemize}
    \item Validate htrdr pp and htrdr sphere on other planets.
    \item Propose a theoritical model to correct pp hypotheses to spherical, multi-scattering configuration.
    \item Analyse using spherical atmosphere for large angle of incidence/emergence -> clouds on the north/south polar.
    \item Analyse the spatial heterogeneity of the atmosphere.
  \end{itemize}

  \hfill

\end{frame}

\appendix
\section{Inversion technique}
\begin{frame}[t]{Inversion}

  Direct model: DISORT -> htrdr

  Comment inversion techniques:
  \begin{itemize}
    \item Levenberg-Marquardt 
      \begin{itemize}
        \item 1st order and 2nd order gradient calculation: FD is not working for htrdr
        \item Potential problem of convergence
        \item Management of standard errors in iterations
      \end{itemize}
    \item Look up table 
      \begin{itemize}
        \item Long calculation time. It may work in far-infrared (no-scattering).
        \item \textcolor{blue}{Symbolic Monte-Carlo}
      \end{itemize}
    \item Bayesian  
      \begin{itemize}
        \item htrdr + MCMC -> \textcolor{blue}{non-linearity Monte Carlo}
        \item sample the priory $p(x)$ -> estimate the posterior $p(x|y_{obs})$, all in one MC
      \end{itemize}
  \end{itemize}

\end{frame}

\section{Symbolic MC}

\begin{frame}[t]{Symbolic MC}

  Monte Carlo: 

  \begin{equation}
    f(x) = \mathbb{E}[W] \approx \sum_{i = 1}^{N} \frac{1}{N} \hat{w}(x_i)
  \end{equation}

  Symbolic Monte Carlo: 

  \begin{equation}
    f(x;y) = \mathbb{E}[W^{\prime}] \cdot g(y) \approx \sum_{i = 1}^{N} \frac{1}{N} \hat{w}^{\prime}(x_i) \cdot g(y)
  \end{equation}
  

\end{frame}

\begin{frame}[t]{Symbolic MC}
  
  \centering
  \includegraphics[width=7cm]{./figures/SMC.png}

\small Penazzi, L�a, et al. "Toward the use of symbolic monte carlo for conduction-radiation coupling in complex geometries." Proceedings of the 9th International Symposium on Radiative Transfer, RAD-19. Begel House Inc., 2019.

\end{frame}

\begin{frame}[t]{Symbolic MC on albedo surface}
  
\centering
\includegraphics[width=5cm]{./figures/najda_albedo.jpg}

Appendix B of:

\small Villefranque, Najda, et al. "A Functionalized Monte Carlo 3D Radiative Transfer Model: Radiative Effects of Clouds Over Reflecting Surfaces." Journal of Advances in Modeling Earth Systems 15.7 (2023): e2023MS003674.

\end{frame}

\section{htrdr inputs}

\begin{frame}[t]{htrdr input generator}
  
\begin{itemize}
  \item Generate the mesh
    \begin{itemize}
      \item ground 
      \item gas 
      \item haze
      \item cloud1, cloud2 ...
    \end{itemize}
  \item Attach the properties to the mesh
  \item Data interpolation 
  \item Observation, Source 
\end{itemize}

\end{frame}



\end{document}


